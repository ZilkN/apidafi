using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiDafi.BOL;
using ApiDafi.BOL.Modelos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiDafi.Controllers
{
    [Authorize]
    public class CariñosasController : BaseApiController
    {
        private readonly ICariñosasRepositorio _cariñosasRepositorio;

        public CariñosasController(ICariñosasRepositorio cariñosasRepositorio)
        {
            _cariñosasRepositorio = cariñosasRepositorio;
        }

        [HttpGet]
        public async Task<IActionResult> GetCariñosas()
        {
            try
            {
                var cariñosas = await _cariñosasRepositorio.ObtenerCariñosas("SELECT * FROM CARIÑOSAS");
                return Ok(cariñosas);
            }
            catch (Exception ex)
            {
                return BadRequest("Puto");
            }
        }

        [HttpPost]
        public async Task<IActionResult> AgregarCariñosa([FromBody] Cariñosa cariñosa)
        {
            try
            {
                await _cariñosasRepositorio.InsertarCariñosa(cariñosa);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest("Puto");

            }
        }
        
        
        
    }
}

