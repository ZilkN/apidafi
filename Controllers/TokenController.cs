using ApiDafi.BOL;
using ApiDafi.BOL.Modelos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiDafi.Controllers;

[AllowAnonymous]
public class TokenController : BaseApiController
{
    private readonly ITokensRepositorio _tokens;

    public TokenController(ITokensRepositorio tokens)
    {
        _tokens = tokens;
    }

    [HttpPost]
    public async Task<IActionResult> Login(Usuario user)
    {
        return Ok(await _tokens.Login(user));
    }
}