using ApiDafi.BOL.Modelos;

namespace ApiDafi.BOL;

public interface ICariñosasRepositorio
{
    Task<List<Cariñosa>> ObtenerCariñosas(string query);
    Task<Cariñosa> ObtenerCariñosaPorId(int Id);
    Task InsertarCariñosa(Cariñosa cariñosa);
}