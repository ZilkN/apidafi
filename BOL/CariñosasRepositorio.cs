using ApiDafi.BOL.Modelos;
using ApiDafi.DAL;
using Dapper;

namespace ApiDafi.BOL;

public class CariñosasRepositorio : ICariñosasRepositorio
{

    private readonly DataConext _context;

    public CariñosasRepositorio(DataConext context)
    {
        _context = context;
    }

    public async Task<List<Cariñosa>> ObtenerCariñosas(string query)
    {
        
        using (var connection = _context.CrearConexion())
        {
            var cariñosas = await connection.QueryAsync<Cariñosa>(query);
            return cariñosas.ToList();
        }
    }

    public Task<Cariñosa> ObtenerCariñosaPorId(int Id)
    {
        throw new NotImplementedException();
    }

    public async Task InsertarCariñosa(Cariñosa cariñosa)
    {
        var query = "INSERT INTO Cariñosas (ID, NombreDeDia, NombreDeNoche) VALUES (@Id, @NombreDeDia, @NombreDeNoche)";
        
        using (var connection = _context.CrearConexion())
        {
            var result = await connection.ExecuteAsync(query, cariñosa);
            
            
        }
    }
}