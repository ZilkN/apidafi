namespace ApiDafi.BOL.Modelos;

public class Cariñosa
{
    public int ID { get; set; }
    public string NombreDeDia { get; set; }
    public string NombreDeNoche { get; set; }
}