using ApiDafi.BOL.Modelos;

namespace ApiDafi.BOL;

public interface ITokensRepositorio
{
    Task<string> Login(Usuario user);

}