using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using ApiDafi.BOL.Modelos;
using Microsoft.IdentityModel.Tokens;

namespace ApiDafi.BOL;

public class TokensRepositorio : ITokensRepositorio
{
    private readonly IConfiguration _configuration;

    public TokensRepositorio(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public async Task<string> Login(Usuario user)
    {
        //Si la validacion es falsa a chingar a su madre.
        if (false)
            return "";
        
        //Validacion de usuario que exista
        var issuer = _configuration["Jwt:Issuer"];
            var audience = _configuration["Jwt:Audience"];
            var key = Encoding.ASCII.GetBytes(_configuration["Jwt:Key"]);


            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject   = new ClaimsIdentity(new []
                {
                   new Claim("Id", "1"),
                   new Claim(JwtRegisteredClaimNames.UniqueName, "ElColorado"),
                   new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }),
                Expires = DateTime.Now.AddDays(1),
                Issuer = issuer,
                Audience = audience,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescription);
            var jwtToken = tokenHandler.WriteToken(token);
            return jwtToken;
        
    }
}